from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
from django.contrib.auth.models import User, Group, check_password
import json
from datetime import datetime
        
from task.models import Lenguajes, Priority, Framework, FirstComponent, SecondComponent, Task, Status, Profile, Cargo, TimeWork, Skill, UserTips, Tips, Sugerencia, EstadisticaUser, Sueldo, SueldoProduc, Productividad
from django.core import serializers 

from django.contrib import auth

        

def index(request):
    template_name = 'task/index.html'
    #
    return render_to_response(
		template_name, 
		{
		}, 
		context_instance=RequestContext(request)
	)

def login(request):
    template_name = 'task/login.html'
    #
    if request.method == 'GET':
        return render_to_response(
            template_name, 
            {
            }, 
            context_instance=RequestContext(request)
        )
    elif request.method == 'POST':
        user = request.POST.get('user',None)
        password = request.POST.get('password',None)
        print user, password
        
        user = authenticate(username=user, password=password)
        if user is not None:
            # the password verified for the user
            auth.login(request, user)
            return HttpResponseRedirect('/home')
        else:
            return render_to_response(
                template_name, 
                { 'error':"Su contrasena o usuario son incorrectos"}, 
                context_instance=RequestContext(request)
            )
    
def home(request):
    template_name = 'task/home.html'
    data={}
    user=request.user

    
    lenguajes = Lenguajes.objects.all()
    data['lenguajes'] = lenguajes
    users = User.objects.filter(groups__name='Empleado')
    data['users'] = users    
    prioritys = Priority.objects.all()
    data['prioritys'] = prioritys    
    first_componets = FirstComponent.objects.all()
    data['first_componets'] = first_componets   
    second_componets = SecondComponent.objects.all()
    data['second_componets'] = second_componets    
    frameworks = Framework.objects.all()
    data['frameworks'] = frameworks   
    
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    if empleado:
        data['group']='Empleado'    
    elif gerente:
        data['group']='Gerente'
        return HttpResponseRedirect('/reports/')
        
    
    
    
    tasks_todo = Task.objects.filter(status_id=1)
    tasks_process = Task.objects.filter(status_id=2)
    tasks_end = Task.objects.filter(status_id=3)
    data['tasks_todo'] = tasks_todo
    data['tasks_process'] = tasks_process
    data['tasks_end'] = tasks_end
    
    profile = Profile.objects.filter(user_id=request.user.pk)
    if profile:
        profile = profile[0]
        
        if profile.cargo:

            tips = Tips.objects.filter(cargo=profile.cargo)

            #userTip = UserTips(user=user, cargo=profile.cargo, tips=tips[0], read=0, saved=0)
            #userTip.save()
            #data['user_tips'] = userTip

            for i in tips:
                userTips = UserTips.objects.filter(user=user, tips=i)
                if not userTips:
                    userTip = UserTips(user=user, cargo=profile.cargo, tips=i, read=0, saved=0)
                    userTip.save()
                    data['user_tips'] = userTip
                    break
    
    print Task.objects.all().values()
    print Status.objects.all().values()

    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    )   

def profile(request):
    
    template_name = 'task/profile.html'
    data={}
    user=request.user
    
    data['user'] = request.user
    profile = Profile.objects.filter(user_id=request.user.pk)
    if profile:
        data['profile'] = profile[0]

    data['cargos'] = Cargo.objects.all()
    
    lenguajes = Lenguajes.objects.all()
    data['lenguajes'] = lenguajes    
    
    frameworks = Framework.objects.all()
    data['frameworks'] = frameworks
    
    times = TimeWork.objects.all()
    data['times'] = times         
    
    regular = Skill.objects.filter(user_id=request.user.pk, know=1)
    data['regular'] = regular      
    bueno = Skill.objects.filter(user_id=request.user.pk, know=2)
    data['bueno'] = bueno      
    muybueno = Skill.objects.filter(user_id=request.user.pk, know=3)
    data['muybueno'] = muybueno      
    excelente = Skill.objects.filter(user_id=request.user.pk, know=4)
    data['excelente'] = excelente   
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    tasks_todo = Task.objects.filter(status_id=1)
    data['tasks_todo'] = tasks_todo
    
    
    if empleado:
        data['group']='Empleado'    
    elif gerente:
        data['group']='Gerente'
    
    
    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    )   


def tips(request):
    template_name = 'task/tips.html'
    data={}
    user=request.user
    
    tasks_todo = Task.objects.filter(status_id=1)
    data['tasks_todo'] = tasks_todo
    
    profile = Profile.objects.filter(user=request.user)
    if profile:
        profile = profile[0]
        
        tips = Tips.objects.filter(cargo=profile.cargo)

        userTips = UserTips.objects.filter(user=user, saved=1)

        data['user_tips'] = userTips
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    if empleado:
        data['group']='Empleado'    
    elif gerente:
        data['group']='Gerente'
    
    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    ) 


def addAporte(request):
    try:
        print 'holaaa', request.POST
        sector = request.POST['sector']
        user_id = request.POST['user_id']
        aporte = request.POST['aporte']
        user=request.user
        
        if 'anonimo' in request.POST:
            anonimo = 1
        else:
            anonimo = 0
            
        print '---->', sector, user_id, aporte, anonimo
        sugerencia = Sugerencia(user_id=user_id, sector=sector, aporte=aporte, anonimo=anonimo)

        sugerencia.save()
        
        empleado = user.groups.filter(name='Empleado').exists()
        gerente = user.groups.filter(name='Gerente').exists()

        if empleado:
            data['group']='Empleado'    
        elif gerente:
            data['group']='Gerente'
        tasks_todo = Task.objects.filter(status_id=1)
        data['tasks_todo'] = tasks_todo

    except Exception,e: 
        print str(e)

    return HttpResponseRedirect('/home/')



def idea(request):
    template_name = 'task/idea.html'
    data={}
    user=request.user
    
        

    tasks_todo = Task.objects.filter(status_id=1)
    data['tasks_todo'] = tasks_todo
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    if empleado:
        data['group']='Empleado' 
        sugerencias = Sugerencia.objects.filter(user=user)
        data['sugerencias'] = sugerencias
    elif gerente:
        data['group']='Gerente'
        sugerencias = Sugerencia.objects.all()
        data['sugerencias'] = sugerencias
        data['users'] = User.objects.all()
    
    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    ) 

def reports(request):
    template_name = 'task/reports.html'
    data={}
    user=request.user
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    
    if empleado:
        data['group']='Empleado'    
    elif gerente:
        data['group']='Gerente'
    
    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    ) 

def users(request):
    template_name = 'task/users.html'
    data={}
    user=request.user
    
    empleado = user.groups.filter(name='Empleado').exists()
    gerente = user.groups.filter(name='Gerente').exists()
    
    profiles = Profile.objects.filter(user__groups__name='Empleado')
    data['profiles'] = profiles    
    

    
    estadistica_user_array= []
    sueldo_produc= []
    sueldos= []
    productividadU1= Productividad.objects.filter(user=profiles[0].user)
    productividadU2= Productividad.objects.filter(user=profiles[1].user)
    
    sueldoPU1= SueldoProduc.objects.filter(user=profiles[0].user)
    sueldoPU2= SueldoProduc.objects.filter(user=profiles[1].user)
    
    data['productividadU1'] = productividadU1
    data['productividadU2'] = productividadU2
    
    data['sueldoPU1'] = sueldoPU1
    data['sueldoPU2'] = sueldoPU2
    
    for profile in profiles:
        estadistica_user = EstadisticaUser.objects.filter(user=profile.user)
        if estadistica_user:
            estadistica_user_array = estadistica_user_array + [estadistica_user[0].cargo_sugerido.name] 
        else:
            estadistica_user_array = estadistica_user_array + [profile.cargo.name]
            
        #sueldo_produc = sueldo_produc + [SueldoProduc.objects.filter(user=profile.user).order_by('date').values_list('amount', flat=True)]
        
        #a = SueldoProduc(user=profile.user, amount=300000)
        #a.save()
        
        sueldo = Sueldo.objects.filter(user=profile.user)
        print '----->', profile.user, sueldo
        if sueldo:
            sueldos = sueldos + [sueldo[0].amount] #cambiar a que si un mes no tiene sueldo agarre el sueldo del mes anterior
        else:
            sueldos = sueldos + [0] #cambiar a que si un mes no tiene sueldo agarre el sueldo del mes anterior
        
    print estadistica_user_array
    print sueldo_produc
    
    data['estadistica_users'] = estadistica_user_array
    data['sueldos'] = sueldos
    #data['sueldo_produc'] = sueldo_produc
    
    if empleado:
        data['group']='Empleado'    
    elif gerente:
        data['group']='Gerente'
        
    print User.objects.all().values()
    
    return render_to_response(
        template_name, 
        data, 
        context_instance=RequestContext(request)
    ) 


def logout(request):
    auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/login/")



def addTask_ajax(request):
    if request.is_ajax():
        language = request.POST.get('language',None)
        title = request.POST.get('title',None)
        description = request.POST.get('description',None)
        datetimepicker1 = request.POST.get('datetimepicker1',None)
        datetimepicker2 = request.POST.get('datetimepicker2',None)
        assign = request.POST.get('assign',None)
        priority = request.POST.get('priority',None)
        component = request.POST.getlist('component[]', None) 
        action = request.POST.getlist('action[]', None)
        framework = request.POST.getlist('framework[]', None)
        print component
            
        try:
            user = User.objects.get(id=assign)
            print user, Lenguajes.objects.filter(id=1)
            language = Lenguajes.objects.get(pk=int(language))
            print language, priority
            priority = Priority.objects.get(pk=priority)
            status = Status.objects.get(pk=1)
            print priority

            date1=datetime.strptime(datetimepicker1, '%m/%d/%Y').date()
            date2=datetime.strptime(datetimepicker2, '%m/%d/%Y').date()
        
        
            task = Task(language=language, name=title, description=description, ini_date=date1, end_date=date2, user=user, priority=priority)

            print task

            task.save()

            print component
            for i in component:
                print i
                firstComponent = FirstComponent.objects.get(name=i)
                print firstComponent
                task.firstComponent.add(firstComponent)        

            print '1-----'
            for i in action:
                print i
                secondComponent = SecondComponent.objects.get(name=i)
                print secondComponent
                task.secondComponent.add(secondComponent)  

            print '1-----'

            for i in framework:
                print i
                framework = Framework.objects.get(name=i)
                print framework
                task.framework.add(framework)

            task.save()
        except Exception,e: 
            print str(e)
            return HttpResponse(json.dumps({'status':'Error'}), content_type = "application/json")

        #a1.publications.add(p1)
        return HttpResponse(json.dumps({'status':'OK'}), content_type = "application/json")
    else:
        raise Http404
        
        
def addStatus_ajax(request):
    if request.is_ajax():
        try:
            task_id = request.POST.get('task_id',None)
            status_id = request.POST.get('status_id',None)
            ranking = request.POST.get('score',None)
            
            print ranking
            print status_id
            print task_id
            

            task = Task.objects.get(id=task_id)
            status = Status.objects.get(id=status_id)


            
            task.status = status
            if ranking:
                task.ranking = ranking
                
            task.save()
        except Exception,e: 
            print str(e)
    
        return HttpResponse(json.dumps({'status':'OK'}), content_type = "application/json")
    else:
        raise Http404  
        

        
def addProfile_ajax(request):
    print 'ADD profile'
    if request.is_ajax():
        try:
            user_id = request.POST.get('user',None)
            position = request.POST.get('position',None)
            name = request.POST.get('name',None)
            time = request.POST.get('time',None)
            lastname = request.POST.get('lastname',None)
            email = request.POST.get('email',None)
            amount = request.POST.get('amount',None)
            regular = request.POST.getlist('regular[]',None)
            bueno = request.POST.getlist('bueno[]',None)
            muybueno = request.POST.getlist('muybueno[]',None)
            excelente = request.POST.getlist('excelente[]',None)
            
            profile = Profile.objects.filter(user_id=user_id)
            user = User.objects.get(id=user_id)
            cargo = Cargo.objects.get(id=position)
            time = TimeWork.objects.get(id=time)
            
            print profile
            if not profile: 
                sueldo = Sueldo(user=user, amount=amount)
                sueldo.save()
                
                profile = Profile(user = user, sueldo=sueldo, cargo = cargo, time = time)  
                profile.save()
            else: 
                profile = profile[0]
                
                sueldo = Sueldo.objects.filter(user=user).order_by('-date')
                sueldo = sueldo[0]
                if not (int(sueldo.amount) == int(ammount)):
                    sueldo = Sueldo(user=user, amount=amount)
                    sueldo.save()
                          
                profile.sueldo = amount
                profile.cargo = cargo
                profile.time = time
                profile.save()
                

                
            for i in regular:
                print 'regular', i
                lenguaje = Lenguajes.objects.filter(name = i)
                if lenguaje:
                    lenguaje = lenguaje[0]
                    skill = Skill.objects.filter(user=user, lenguaje=lenguaje, know=1)
                    if not skill:
                        skill = Skill.objects.filter(user=user, lenguaje=lenguaje)
                        if skill: 
                            skill.know = 1
                            skill.save()
                        else:

                            skill = Skill(user=user, lenguaje=lenguaje, know=1)
                            skill.save()
                else:
                    framework = Framework.objects.get(name = i)
                    skill = Skill.objects.filter(user=user, framework=framework, know=1)
                    if not skill:
                        skill = Skill.objects.filter(user=user, framework=framework)
                        if skill: 
                            skill.know = 1
                            skill.save()
                        else:

                            skill = Skill(user=user, framework=framework, know=1)
                            skill.save()

                for i in bueno:
                    print 'bueno', i
                    lenguaje = Lenguajes.objects.filter(name = i)
                    if lenguaje:
                        lenguaje = lenguaje[0]
                        skill = Skill.objects.filter(user=user, lenguaje=lenguaje, know=2)
                        if not skill:
                            skill = Skill.objects.filter(user=user, lenguaje=lenguaje)
                            if skill: 
                                skill.know = 2
                                skill.save()
                            else:

                                skill = Skill(user=user, lenguaje=lenguaje, know=2)
                                skill.save()
                    else:
                        framework = Framework.objects.get(name = i)
                        skill = Skill.objects.filter(user=user, framework=framework, know=2)
                        if not skill:
                            skill = Skill.objects.filter(user=user, framework=framework)
                            if skill: 
                                skill.know = 2
                                skill.save()
                            else:

                                skill = Skill(user=user, framework=framework, know=2)
                                skill.save()
                                
                for i in muybueno:
                    print 'muybueno', i
                    
                    lenguaje = Lenguajes.objects.filter(name = i)
                    if lenguaje:
                        lenguaje = lenguaje[0]
                        skill = Skill.objects.filter(user=user, lenguaje=lenguaje, know=3)
                        if not skill:
                            skill = Skill.objects.filter(user=user, lenguaje=lenguaje)
                            if skill: 
                                skill.know = 3
                            else:

                                skill = Skill(user=user, lenguaje=lenguaje, know=3)
                                skill.save()
                    else:
                        framework = Framework.objects.get(name = i)
                        skill = Skill.objects.filter(user=user, framework=framework, know=3)
                        if not skill:
                            skill = Skill.objects.filter(user=user, framework=framework)
                            if skill: 
                                skill.know = 3
                                skill.save()
                            else:

                                skill = Skill(user=user, framework=framework, know=3)
                                skill.save()
                for i in excelente:
                    
                    print 'excelente', i
                    
                    lenguaje = Lenguajes.objects.filter(name = i)
                    if lenguaje:
                        lenguaje = lenguaje[0]
                        skill = Skill.objects.filter(user=user, lenguaje=lenguaje, know=4)
                        if not skill:
                            skill = Skill.objects.filter(user=user, lenguaje=lenguaje)
                            if skill: 
                                skill.know = 4
                                skill.save()
                            else:

                                skill = Skill(user=user, lenguaje=lenguaje, know=4)
                                skill.save()
                    else:
                        framework = Framework.objects.get(name = i)
                        skill = Skill.objects.filter(user=user, framework=framework, know=4)
                        if not skill:
                            skill = Skill.objects.filter(user=user, framework=framework)
                            if skill: 
                                skill.know = 4
                                skill.save()
                            else:

                                skill = Skill(user=user, framework=framework, know=4)
                                skill.save()

        except Exception,e: 
            print str(e)
    
        return HttpResponse(json.dumps({'status':'OK'}), content_type = "application/json")
    else:
        raise Http404
        
        
        
def addFavorite_ajax(request):
    if request.is_ajax():
        try:
            user_tips_id = request.POST.get('user_tips_id',None)
            favorite = request.POST.get('favorite',None)
            
            user_tips = UserTips.objects.get(id=user_tips_id)
            user_tips.saved = favorite
            user_tips.save()
            
        except Exception,e: 
            print str(e)
    
        return HttpResponse(json.dumps({'status':'OK'}), content_type = "application/json")
    else:
        raise Http404  
        


    