from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'index/^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^home/$', views.home, name='home'),
    url(r'^addTask/$', views.addTask_ajax, name='addTask_ajax'),
    url(r'^addStatus/$', views.addStatus_ajax, name='addStatus_ajax'),
    url(r'^addProfile/$', views.addProfile_ajax, name='addProfile_ajax'),
    url(r'^addFavorite/$', views.addFavorite_ajax, name='addFavorite_ajax'),
    url(r'^addAporte/$', views.addAporte, name='addAporte'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^tips/$', views.tips, name='tips'),
    url(r'^idea/$', views.idea, name='idea'),
    url(r'^reports/$', views.reports, name='reports'),
    url(r'^users/$', views.users, name='users'),
    url(r'^logout/$', views.logout, name='logout'),
]