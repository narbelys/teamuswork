# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0012_sugerencia'),
    ]

    operations = [
        migrations.AddField(
            model_name='sugerencia',
            name='status',
            field=models.IntegerField(default=0),
        ),
    ]
