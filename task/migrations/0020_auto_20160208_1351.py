# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0019_project'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='task',
        ),
        migrations.AddField(
            model_name='task',
            name='project',
            field=models.ForeignKey(to='task.Project', null=True),
        ),
    ]
