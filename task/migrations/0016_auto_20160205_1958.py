# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0015_auto_20160205_1947'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sueldo',
            old_name='sueldo',
            new_name='amount',
        ),
    ]
