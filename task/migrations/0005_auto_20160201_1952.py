# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0004_framework'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='task',
            name='firstComponent',
            field=models.ManyToManyField(to='task.FirstComponent'),
        ),
        migrations.AddField(
            model_name='task',
            name='framework',
            field=models.ManyToManyField(to='task.Framework'),
        ),
        migrations.AddField(
            model_name='task',
            name='secondComponent',
            field=models.ManyToManyField(to='task.SecondComponent'),
        ),
        migrations.AddField(
            model_name='task',
            name='status',
            field=models.ForeignKey(to='task.Status', null=True),
        ),
    ]
