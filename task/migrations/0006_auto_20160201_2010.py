# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0005_auto_20160201_1952'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='language',
            field=models.ForeignKey(to='task.Lenguajes', null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.ForeignKey(default=0, to='task.Status'),
        ),
    ]
