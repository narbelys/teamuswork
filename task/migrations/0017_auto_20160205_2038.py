# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0016_auto_20160205_1958'),
    ]

    operations = [
        migrations.CreateModel(
            name='SueldoProduc',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField(default=0)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='estadisticauser',
            name='sueldo_sugerido',
        ),
        migrations.AlterField(
            model_name='sueldo',
            name='date',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
