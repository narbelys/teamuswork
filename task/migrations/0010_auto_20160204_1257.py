# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0009_auto_20160204_1255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='framework',
            field=models.ForeignKey(to='task.Framework', null=True),
        ),
        migrations.AlterField(
            model_name='skill',
            name='lenguaje',
            field=models.ForeignKey(to='task.Lenguajes', null=True),
        ),
    ]
