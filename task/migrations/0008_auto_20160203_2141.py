# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0007_auto_20160203_1910'),
    ]

    operations = [
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('know', models.IntegerField(default=1)),
                ('framework', models.ForeignKey(to='task.Framework')),
                ('lenguajes', models.ForeignKey(to='task.Lenguajes')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='profile',
            name='framework',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='lenguajes',
        ),
    ]
