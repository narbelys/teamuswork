# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0008_auto_20160203_2141'),
    ]

    operations = [
        migrations.RenameField(
            model_name='skill',
            old_name='lenguajes',
            new_name='lenguaje',
        ),
    ]
