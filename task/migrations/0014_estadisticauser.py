# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0013_sugerencia_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='EstadisticaUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sueldo_sugerido', models.IntegerField(default=0)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('cargo_sugerido', models.ForeignKey(to='task.Cargo')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
