# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0006_auto_20160201_2010'),
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TimeWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='framework',
            field=models.ForeignKey(default=1, to='task.Framework'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='lenguajes',
            field=models.ForeignKey(default=1, to='task.Lenguajes'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.ForeignKey(default=1, to='task.Status'),
        ),
        migrations.AddField(
            model_name='profile',
            name='cargo',
            field=models.ForeignKey(default=1, to='task.Cargo'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='time',
            field=models.ForeignKey(default=1, to='task.TimeWork'),
            preserve_default=False,
        ),
    ]
