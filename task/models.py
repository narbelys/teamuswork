from django.db import models

from django.conf import settings
from django.contrib.auth.models import User

class Profile(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    sueldo = models.ForeignKey('Sueldo')
    cargo = models.ForeignKey('Cargo')
    time = models.ForeignKey('TimeWork')
    
    def __unicode__(self):
        return self.user.username

    
class Lenguajes(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class Priority(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class Status(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class FirstComponent(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class SecondComponent(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class Framework(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class Area(models.Model):
    name = models.CharField(max_length=200)

class Cargo(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class TimeWork(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class Skill(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                    on_delete=models.SET_NULL)
    lenguaje = models.ForeignKey('Lenguajes', null=True)
    
    framework = models.ForeignKey('Framework', null=True)
    
    know = models.IntegerField(default=1)
    
  
class Project(models.Model):
    name = models.CharField(max_length=200)

class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    ranking = models.IntegerField(('Ranking'), default=0)
    #user = models.ForeignKey(User)
    
    ini_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    priority = models.ForeignKey('Priority', null=True)
    status = models.ForeignKey(Status, default=1)
    language = models.ForeignKey(Lenguajes, null=True)
    firstComponent = models.ManyToManyField('FirstComponent')
    secondComponent = models.ManyToManyField('SecondComponent')
    framework = models.ManyToManyField('Framework')
    project = models.ForeignKey('Project', null=True)
    
    
class Tips(models.Model):
    name = models.CharField(max_length=200)
    cargo = models.ForeignKey('Cargo')
    url = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    
class UserTips(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    cargo = models.ForeignKey('Cargo')
    
    tips = models.ForeignKey('Tips')
    
    read = models.IntegerField(default=0)
    
    saved = models.IntegerField(default=0)
    
    
class Sugerencia(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    aporte = models.CharField(max_length=200)
    sector = models.CharField(max_length=200)
    anonimo = models.IntegerField(default=1)
    status = models.IntegerField(default=0)
    
    
class EstadisticaUser(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    cargo_sugerido = models.ForeignKey('Cargo')
    
    date = models.DateTimeField(null=True, blank=True)
    
    
class Sueldo(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    amount = models.IntegerField(default=0)
    
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    
    
class SueldoProduc(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    amount = models.IntegerField(default=0)
    
    date = models.DateTimeField(null=True, blank=True)
    
class Productividad(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
                            on_delete=models.SET_NULL)
    
    amount = models.IntegerField(default=0)
    
    date = models.DateTimeField(auto_now_add=True, null=True, blank=True)


    #(language=language, title=title, description=description, ini_date=datetimepicker1, end_date=datetimepicker2, user=user, priority=priority