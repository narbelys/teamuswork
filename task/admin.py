from django.contrib import admin

from django.contrib import admin

from .models import Profile, Task, Lenguajes, Priority, FirstComponent, SecondComponent, Framework, Status, Cargo, TimeWork, Skill, Tips, UserTips, Sugerencia, EstadisticaUser, Sueldo, SueldoProduc, Productividad, Project

    
class TaskAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "description", "ranking", "ini_date", "end_date", "user", "priority", "status", "language"]
    
class UserTipsAdmin(admin.ModelAdmin):
    list_display = ["id", "user", "cargo", "tips", "read", "saved"]

admin.site.register(Profile)
admin.site.register(Task, TaskAdmin)
admin.site.register(Lenguajes)
admin.site.register(Priority)
admin.site.register(Status)
admin.site.register(FirstComponent)
admin.site.register(SecondComponent)
admin.site.register(Framework)
admin.site.register(Cargo)
admin.site.register(TimeWork)
admin.site.register(Skill)
admin.site.register(Tips)
admin.site.register(UserTips, UserTipsAdmin)
admin.site.register(Sugerencia)
admin.site.register(EstadisticaUser)
admin.site.register(Sueldo)
admin.site.register(SueldoProduc)
admin.site.register(Productividad)
admin.site.register(Project)
